package com.example.activitat_listview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ReservaActivity extends AppCompatActivity {
    private SwitchCompat switchPensio;
    private RadioGroup radioPensio;
    private RadioButton rbCap;
    private RadioButton rbVeg;
    private RadioButton rbSense;
    private RadioButton rbCeliac;
    private TextView dataEntrada;
    private TextView dataSortida;
    private Spinner spinner;
    private ImageView imgReserva;
    private TextInputEditText textNom;
    private TextInputEditText textCognoms;
    private TextInputEditText textEmail;
    private TextInputEditText textNumPer;
    private Button btReserva;

    public static final String FILE_SHARED_NAME = "MySharedFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        switchPensio = findViewById(R.id.switchPensio);
        radioPensio = findViewById(R.id.radioPensio);
        rbCap = findViewById(R.id.rbCap);
        rbVeg = findViewById(R.id.rbVeg);
        rbSense = findViewById(R.id.rbSense);
        rbCeliac = findViewById(R.id.rbCeliac);
        dataEntrada = findViewById(R.id.dataEntrada);
        dataSortida = findViewById(R.id.dataSortida);
        textNom = findViewById(R.id.textNom);
        textCognoms = findViewById(R.id.textCognoms);
        textEmail = findViewById(R.id.textEmail);
        textNumPer = findViewById(R.id.textNumPer);
        imgReserva = findViewById(R.id.imgReserva);
        spinner = findViewById(R.id.spinner);
        btReserva = findViewById(R.id.btReserva);

        loadData();
        if (rbCap.isChecked()) {
            switchPensio.setChecked(false);
            enableDisableRG(radioPensio, false);
        }

        List<String> sParcs = Arrays.asList(
                "Selecciona Refugi",
                "Refugi Josep Maria Blanc",
                "Refugi Cap de Llauset",
                "Refugi Ventosa i Clavell",
                "Refugi Amitges",
                "Refugi Josep Maria Montfort");

        ArrayAdapter<String> parcsAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_activated_1,
                sParcs
        );
        spinner.setAdapter(parcsAdapter);

        List<Integer> imgResource = Arrays.asList(0,
                R.drawable.foto1,
                R.drawable.foto2,
                R.drawable.foto3,
                R.drawable.foto4,
                R.drawable.foto5 );


        Intent intent = getIntent();
        String titolRefugi = intent.getStringExtra(DashboardActivity.TITOL_PARC);
        int imatgeRefugi = intent.getIntExtra(DashboardActivity.IMATGE_PARC, 0);
        imgReserva.setImageResource(imatgeRefugi);
        int index = sParcs.indexOf(titolRefugi);
        spinner.setSelection(index);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                imgReserva.setImageResource(imgResource.get(position));
                String selectedParc = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        switchPensio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    enableDisableRG(radioPensio, true);
                } else {
                    enableDisableRG(radioPensio, false);
                    rbCap.setChecked(true);
                }
            }
        });

        dataEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(dataEntrada).show();
            }
        });

        dataSortida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog(dataSortida).show();
            }
        });

        //Reserva amb mail

//        btReserva.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String[] TO = {textEmail.getText().toString()};
//                String[] CC = {textEmail.getText().toString()};
//                String subject = "Reserva Parcs";
//                String text = "Nom: " + textNom.getText() +
//                    "\nRefugi: " + spinner.getSelectedItem() +
//                    "\nEmail: " + textEmail.getText() +
//                    "\nNum Persones: " + textNumPer.getText() +
//                    "\nPensio Completa: " + getTipusPensio(radioPensio) +
//                    "\nData Entrada: " + dataEntrada.getText()+
//                    "\nData Sortida: " + dataSortida.getText();
//                sendMail(TO, CC, subject, text);
//            }
//        });

        btReserva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });

    }

    public String getTipusPensio(RadioGroup radioGroup){
        int rbId = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = findViewById(rbId);
        if (radioButton.getText().toString().equals("Cap")){
            return "NO";
        } else {
            return "SI\nMenu: " + radioButton.getText().toString();
        }
    }

    private void sendMail(String[] TO, String[] CC, String subject, String text) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(emailIntent,"Send mail..."));
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(ReservaActivity.this, "There is no email client installed", Toast.LENGTH_SHORT).show();
        }
    }


    private void enableDisableRG(RadioGroup radioGroup, boolean isEnable){
        for(int i = 0; i < radioGroup.getChildCount(); i++){
            radioGroup.getChildAt(i).setClickable(isEnable);
        }
    }

    private DatePickerDialog datePickerDialog(TextView textData){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        DatePickerDialog picker = new DatePickerDialog(ReservaActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month++;
                        textData.setText(dayOfMonth + "/" + month + "/" + year);
                    }
                }, year, month, day);
        return picker;
    }

    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("name", textNom.getText().toString());
        editor.putString("lastname", textCognoms.getText().toString());
        editor.putString("email", textEmail.getText().toString());
        editor.putString("numberpeople", textNumPer.getText().toString());
        String isPensionChecked = null;
        if(switchPensio.isChecked()) {
            isPensionChecked = "true";
            editor.putInt("pensionValue", radioPensio.getCheckedRadioButtonId());
        }
        editor.putString("pension", isPensionChecked);

        editor.commit();
        Toast.makeText(ReservaActivity.this, "Data saved", Toast.LENGTH_SHORT).show();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(FILE_SHARED_NAME, Context.MODE_PRIVATE);
        String sNom = sharedPreferences.getString("name", "");
        String sCognoms = sharedPreferences.getString("lastname", "");
        String sMail = sharedPreferences.getString("email", "");
        String sNumberPeople = sharedPreferences.getString("numberpeople", "");
        String isPension = sharedPreferences.getString("pension", "");

        textNom.setText(sNom);
        textCognoms.setText(sCognoms);
        textEmail.setText(sMail);
        textNumPer.setText(sNumberPeople);

        if (isPension.equals("true")) {
            switchPensio.setChecked(true);
            int selectedRadioButtonId = sharedPreferences.getInt("pensionValue", 0);
            RadioButton rbTmp = findViewById(selectedRadioButtonId);
            rbTmp.setChecked(true);
        }

    }

}