package com.example.activitat_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageViewLogo2;
    private ImageView imageViewLogo1;
    private LinearLayout linearMain;
    private TextView textTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewLogo1 = findViewById(R.id.logo1);
        imageViewLogo2 = findViewById(R.id.logo2);
        linearMain = findViewById(R.id.linearMain);
        textTitle = findViewById(R.id.textTitle);

        imageViewLogo1.setTranslationX(-2000);
        imageViewLogo1.setAlpha(0f);
        imageViewLogo1.animate().translationX(0f).alpha(1).setDuration(2000);

        linearMain.setTranslationX(2000);
        linearMain.setAlpha(0f);
        linearMain.animate().translationX(0f).alpha(1).setDuration(2000);


        imageViewLogo2.setAlpha(0f);
        imageViewLogo2.animate().alpha(1).setStartDelay(2000).setDuration(2000);

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(imageViewLogo1, "imageapp");
                pairs[1] = new Pair<View, String>(textTitle, "textapp");
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                startActivity(intent, options.toBundle());
            }
        },4000);
    }
}