package com.example.activitat_listview;

public class Item {
    private String titolRefugi;
    private String ubicacio;
    private String comarca;
    private String ccaa;
    private String distancia;
    private String desnivell;
    private String altura;
    private String llit;
    private boolean isOpened;
    private int imatge;

    public String getTitolRefugi() {
        return titolRefugi;
    }

    public void setTitolRefugi(String titolRefugi) {
        this.titolRefugi = titolRefugi;
    }

    public String getUbicacio() {
        return ubicacio;
    }

    public void setUbicacio(String ubicacio) {
        this.ubicacio = ubicacio;
    }

    public String getComarca() {
        return comarca;
    }

    public void setComarca(String comarca) {
        this.comarca = comarca;
    }

    public String getCcaa() {
        return ccaa;
    }

    public void setCcaa(String ccaa) {
        this.ccaa = ccaa;
    }

    public String getDistancia() {
        return distancia;
    }

    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }

    public String getDesnivell() {
        return desnivell;
    }

    public void setDesnivell(String desnivell) {
        this.desnivell = desnivell;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getLlit() {
        return llit;
    }

    public void setLlit(String llit) {
        this.llit = llit;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        this.isOpened = opened;
    }

    public int getImatge() {
        return imatge;
    }

    public void setImatge(int imatge) {
        this.imatge = imatge;
    }

    public Item(String titolRefugi, String ubicacio, String comarca, String ccaa, String distancia, String desnivell, String altura, String llit, boolean horari, int imatge) {
        this.titolRefugi = titolRefugi;
        this.ubicacio = ubicacio;
        this.comarca = comarca;
        this.ccaa = ccaa;
        this.distancia = distancia;
        this.desnivell = desnivell;
        this.altura = altura;
        this.llit = llit;
        this.isOpened = horari;
        this.imatge = imatge;
    }
}
