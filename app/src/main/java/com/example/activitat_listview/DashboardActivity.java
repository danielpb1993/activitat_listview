package com.example.activitat_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {
    public static String IMATGE_PARC = "com.example.activitat_listview.IMATGE_PARC";
    public static String TITOL_PARC = "com.example.activitat_listview.TITOL_PARC";
    private ListView listView;

    Item item1 = new Item("Refugi Josep Maria Blanc", "Parc Aigüestortes", "Vall d'Aran", "Catalunya", "Distància: 2:30h",
            "Desnivell: 1200m", "2.100m", "30", true, R.drawable.foto1);
    Item item2 = new Item("Refugi Cap de Llauset", "Posets-Maladeta", "Osca", "Aragó", "Distància: 2:15h",
            "Desnivell: 1100m", "2.800m", "25", true, R.drawable.foto2);
    Item item3 = new Item("Refugi Ventosa i Clavell", "Parc Aigüestortes", "Vall d'Aran", "Catalunya", "Distància: 3:15h",
            "Desnivell: 800m", "2.150m", "45", true, R.drawable.foto3);
    Item item4 = new Item("Refugi Amitges", "Parc Aigüestortes", "Vall d'Aran", "Catalunya", "Distància: 2:30h",
            "Desnivell: 750m", "2.400m", "45", false, R.drawable.foto4);
    Item item5 = new Item("Refugi Josep Maria Montfort", "Alt Pirineu", "Vall Ferrera", "Catalunya", "Distància: 2:30h",
            "Desnivell; 950m", "1.875m", "23", true, R.drawable.foto5);

    ArrayList<Item> items = new ArrayList<>();
    CustomAdapter customAdapter = new CustomAdapter(this, items);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        listView = findViewById(R.id.listView);
        listView.setAdapter(customAdapter);

        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);
        items.add(item5);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                int imatgeParc = items.get(position).getImatge();
                String titolParc = items.get(position).getTitolRefugi();
                intent.putExtra(IMATGE_PARC, imatgeParc);
                intent.putExtra(TITOL_PARC, titolParc);
                startActivity(intent);
            }
        });


    }

    private class CustomAdapter extends BaseAdapter {
        private Context context;
        private List<Item> items;

        public CustomAdapter(Context context, ArrayList<Item> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row2, null);

            TextView titolRegugi = view.findViewById(R.id.titolRefugi);
            ImageView imatgeRefugi = view.findViewById(R.id.imatgeRefugi);
            TextView ubicacio = view.findViewById(R.id.ubicacio);
            TextView comarca = view.findViewById(R.id.comarca);
            TextView ccaa = view.findViewById(R.id.ccaa);
            TextView distancia = view.findViewById(R.id.distancia);
            TextView desnivell = view.findViewById(R.id.desnivell);
            TextView altura = view.findViewById(R.id.altura);
            TextView llit = view.findViewById(R.id.llit);
            TextView isOpened = view.findViewById(R.id.isOpened);

            titolRegugi.setText(items.get(position).getTitolRefugi());
            imatgeRefugi.setImageResource(items.get(position).getImatge());
            ubicacio.setText(items.get(position).getUbicacio());
            comarca.setText(items.get(position).getComarca());
            ccaa.setText(items.get(position).getCcaa());
            distancia.setText(items.get(position).getDistancia());
            desnivell.setText(items.get(position).getDesnivell());
            altura.setText(items.get(position).getAltura());
            llit.setText(items.get(position).getLlit());

            if (items.get(position).isOpened()) {
                isOpened.setText("Open");
            } else {
                isOpened.setText("Close");
                isOpened.setTextColor(Color.parseColor("#FF0000"));
            }

            return view;
        }
    }
}